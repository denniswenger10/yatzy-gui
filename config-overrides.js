const {
  override,
  addWebpackAlias,
  addBabelPresets,
} = require('customize-cra')
const path = require('path')

module.exports = override(
  addWebpackAlias({
    ['@components']: path.resolve(__dirname, './src/components'),
    ['@pure']: path.resolve(__dirname, './src/components/pure'),
    ['@src']: path.resolve(__dirname, './src'),
    ['@pages']: path.resolve(__dirname, './src/pages'),
    ['@styles']: path.resolve(__dirname, './src/styles'),
    ['@utils']: path.resolve(__dirname, './src/utils'),
  }),
  addBabelPresets('@emotion/babel-preset-css-prop')
)
