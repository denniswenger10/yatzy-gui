import React from 'react'
import { css } from '@emotion/core'
import { TEXT_STYLE } from '@styles'

const LandingHeader = () => {
  const styles = css({
    textAlign: 'center',
  })

  return <h1 css={[TEXT_STYLE.heading[2], styles]}>Yatzy</h1>
}

export { LandingHeader }
