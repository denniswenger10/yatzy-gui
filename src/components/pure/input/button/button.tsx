import React, { ReactNode } from 'react'
import { css } from '@emotion/core'

import { buttonReset } from '@styles/utils'
import { COLORS } from '@styles'

import { buttonVariant } from './button-variant'
import { buttonSize } from './button-size'
import {
  getBackgroundColor,
  getTextColor,
  getTextStyle,
  getPadding,
  getBorderRadius,
} from './button-utils'

type ButtonProps = {
  children?: ReactNode
  variant?: string
  size?: string
}

const Button = ({
  children = [],
  variant = buttonVariant.PRIMARY,
  size = buttonSize.LARGE,
}: ButtonProps) => {
  const styles = css([
    buttonReset,
    getTextStyle(size),
    {
      background: getBackgroundColor(variant),
      color: getTextColor(variant),
      padding: getPadding(size),
      borderRadius: getBorderRadius(size),
      textAlign: 'center',
      position: 'relative',
      transition: 'all 200ms ease',
      WebkitBackfaceVisibility: 'initial',
      MozBackfaceVisibility: 'initial',
      '&::before': {
        content: "''",
        position: 'absolute',
        borderRadius: getBorderRadius(size),
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        opacity: 0,
        transition: 'all 200ms ease',
        boxShadow: `0 0 0 3px ${COLORS.elm[90]} inset`, // TODO: Establish focused state in design
      },
      '&:focus': {
        outline: 'none',
        '&::before': {
          opacity: 1,
        },
      },
      '&::after': {
        content: "''",
        position: 'absolute',
        borderRadius: getBorderRadius(size),
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        background: 'rgba(0, 0, 0, 0.1)',
        transition: 'all 200ms ease',
        opacity: 0,
      },
      '&:hover': {
        transform:
          'scale(0.98) translate3d( 0, 0, 0) perspective(1px)',
        '&::after': {
          opacity: 1,
        },
      },
      '&:active': {
        transform:
          'scale(0.96) translate3d( 0, 0, 0) perspective(1px)',
      },
    },
  ])

  return <button css={styles}>{children}</button>
}

export { Button }
