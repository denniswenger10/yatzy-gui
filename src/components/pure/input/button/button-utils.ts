import { COLORS, TEXT_STYLE } from '@styles'
import { buttonVariant } from './button-variant'
import { buttonSize } from './button-size'

const backgroundColor = new Map([
  [buttonVariant.PRIMARY, COLORS.elm[64]],
])

const textColor = new Map([
  [buttonVariant.PRIMARY, COLORS.white[100]],
])

const textStyle = new Map([
  [buttonSize.SMALL, TEXT_STYLE.button[2]],
  [buttonSize.LARGE, TEXT_STYLE.button[1]],
])

const padding = new Map([
  [buttonSize.SMALL, '16px 24px'],
  [buttonSize.LARGE, '16px 24px'],
])

const borderRadius = new Map([
  [buttonSize.SMALL, '15px'],
  [buttonSize.LARGE, '20px'],
])

const getBackgroundColor = (variant = buttonVariant.PRIMARY) => {
  return (
    backgroundColor.get(variant) ||
    backgroundColor.get(buttonVariant.PRIMARY)
  )
}

const getTextColor = (variant = buttonVariant.PRIMARY) => {
  return (
    textColor.get(variant) || textColor.get(buttonVariant.PRIMARY)
  )
}

const getTextStyle = (size = buttonSize.LARGE) => {
  return textStyle.get(size) || textStyle.get(buttonSize.LARGE)
}

const getPadding = (size = buttonSize.LARGE) => {
  return padding.get(size) || padding.get(buttonSize.LARGE)
}

const getBorderRadius = (size = buttonSize.LARGE) => {
  return borderRadius.get(size) || borderRadius.get(buttonSize.LARGE)
}

export {
  getBackgroundColor,
  getTextColor,
  getTextStyle,
  getPadding,
  getBorderRadius,
}
