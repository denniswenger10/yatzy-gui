const buttonSize = {
  LARGE: 'large',
  SMALL: 'small',
}

export { buttonSize }
