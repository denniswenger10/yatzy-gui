import React from 'react'
import { RouteComponentProps } from '@reach/router'

const NotFound = (props: RouteComponentProps) => {
  return <p>404, not found</p>
}

export { NotFound }
