import React from 'react'
import { RouteComponentProps } from '@reach/router'
import { LandingHeader } from '@pure'
import { Button } from '@pure/input/button'
import { css } from '@emotion/core'

const Home = (props: RouteComponentProps) => {
  const styles = css({
    display: 'grid',
    justifyContent: 'center',
  })

  return (
    <div css={styles}>
      <LandingHeader />
      <Button>Start Game</Button>
    </div>
  )
}

export { Home }
