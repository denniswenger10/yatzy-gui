import React from 'react'
import { render } from 'react-dom'
import { App } from './App'
import './index.css'
import { Global } from '@emotion/core'
import { RESET, GLOBAL } from './styles'

render(
  <React.StrictMode>
    <Global styles={[RESET, GLOBAL]} />
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)
