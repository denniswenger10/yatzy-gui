import { COLORS } from './colors'

const PLAYER_COLORS = [
  COLORS.violet[50],
  COLORS.violet[77],
  COLORS.elm[50],
  COLORS.cerulean[77],
  COLORS.green[64],
  COLORS.yellow[90],
  COLORS.gray[90],
  COLORS.yellow[50],
  COLORS.orange[50],
  COLORS.red[50],
]

export { PLAYER_COLORS }
