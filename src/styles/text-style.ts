import { css } from '@emotion/core'

const TEXT_STYLE = {
  heading: {
    '1': css({
      fontFamily: 'Atma, cursive',
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '96px',
      lineHeight: '104px',
      letterSpacing: '0.04em',
    }),
    '2': css({
      fontFamily: 'Atma, cursive',
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '48px',
      lineHeight: '52px',
      letterSpacing: '0.04em',
    }),
  },
  subtitle: {
    '1': css({
      fontFamily: 'Barlow, sans-serif',
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '16px',
      lineHeight: '20px',
      letterSpacing: '0.02em',
    }),
  },
  body: {
    '1': css({
      fontFamily: 'Barlow, sans-serif',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '20px',
      lineHeight: '34px',
      letterSpacing: '0.02em',
    }),
    '2': css({
      fontFamily: 'Barlow, sans-serif',
      fontStyle: 'normal',
      fontWeight: 'normal',
      fontSize: '16px',
      lineHeight: '25px',
      letterSpacing: '0.02em',
    }),
    '3': css({
      fontFamily: 'Barlow, sans-serif',
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '16px',
      lineHeight: '25px',
      letterSpacing: '0.02em',
    }),
  },
  button: {
    '1': css({
      fontFamily: 'Barlow, sans-serif',
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '30px',
      lineHeight: '34px',
      letterSpacing: '0.02em',
      textTransform: 'uppercase',
    }),
    '2': css({
      fontFamily: 'Barlow, sans-serif',
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: '24px',
      lineHeight: '34px',
      letterSpacing: '0.02em',
      textTransform: 'uppercase',
    }),
  },
}

export { TEXT_STYLE }
