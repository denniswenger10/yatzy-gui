import React from 'react'
import { Router } from '@reach/router'
import { Home, NotFound } from './pages'

const AppRouter = () => (
  <Router>
    <NotFound default />
    <Home path="/" />
  </Router>
)

const App = () => {
  return <AppRouter />
}

export { App }
